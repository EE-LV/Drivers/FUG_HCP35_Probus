﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">Driver for the ProBusV Interface from FUG. It is used as an interface for the HCP 35-20000 power supply at TRIGA-TRAP.

Note: Even if it has the same name as the driver used at ISOLTRAP to control the FUG-HCN power supply, I had to change several things, so it will not work anymore with this PS. </Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,C!!!*Q(C=T:3R&lt;B."%)&lt;`1R2'6#EJE#+\D,NZB22ZA845)UI[N[&amp;"@I8J[,!MI4S!%64JJIE5FXY"#IM+7:(&amp;]&gt;VGD;T9W!U)&gt;G`WTP_`/`P&gt;\PKE5HL35\8(SP/$Z;$@(P&lt;&lt;)`[R]@`;\R&lt;H_0I=+TJ9]!_/[@T\L&lt;+[(^Y0(`N&lt;)`@\NT?X?&amp;W\7J_N&gt;`U&lt;;FP;V:$SW,`&amp;O7V,OTJ&lt;H_XY'Z.WN?\NZN]EJ^XLNR7OY^_8@`P^^P#NNMM?@\DOR.LO]&gt;@TTFI0VL`RBX?&gt;/#D80L_&gt;!T9IV\\^I^T.J=&amp;!=_W54&gt;_\/888VP(T=`TY82U`J(_X3%UB*:))QAEL&lt;^=G?K)H?K)H?K)(?K!(?K!(?K!\OK-\OK-\OK-&lt;OK%&lt;OK%&lt;OK'8CCZUI1O&gt;65EG4S:+EC9*EM[A+'E3HI1HY5FY_+G%*_&amp;*?"+?B)=O3HA3HI1HY5FY'+;%*_&amp;*?"+?B)&gt;5B32,29=HY3'^!J[!*_!*?!)?JF4!%Q!%EQ7*AS2A+$#$BY!HY!FY?&amp;4!%`!%0!&amp;0Q)/NA#@A#8A#HI#()766IN"U&amp;2U?UMDB=8A=(I@(Y3'V("[(R_&amp;R?"Q?JJ0$Y`!Y%-[%4H)1Z!RS/DA`("[(BZM=(I@(Y8&amp;Y("[MME.?6K;D[3I[0!;0Q70Q'$Q'$SFE]"A]"I`"9`#16A;0Q70Q'$Q'$V0*Y$&amp;Y$"Y$R*C5[75E-Q9;H1T"Y/%KJ]8+,E5BM6,L8`0BI+I?1.7$J8JA6!_#[A;L&lt;JTKBKAON/I#KC[-[AOLPIAKI/L%KAF6/WJ*OS"GR*19%S0CEDAH_M2*.`10&gt;VQOFVIM&amp;JL.:JJ/JRK0RRK.2LK]P.4Z_&lt;H[`&lt;Z/4EZ_@;V/K1_FW8S8LHAGHB%P[PVRP/T[&lt;0J_;6@.?UW;[R_4ZO0X340^WC-OGA_P*C7O0V`A^?D4;S;HP?&lt;4'_Y8P4,OX7MV&lt;\_6_:K(\^,`]'X5%\6&lt;HXH7[#&gt;M"#&amp;.!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Probus.ReadCurrent.vi" Type="VI" URL="../Probus.ReadCurrent.vi"/>
		<Item Name="Probus.ReadVoltage.vi" Type="VI" URL="../Probus.ReadVoltage.vi"/>
		<Item Name="Probus.DisableOutput.vi" Type="VI" URL="../Probus.DisableOutput.vi"/>
		<Item Name="Probus.EnableOutput.vi" Type="VI" URL="../Probus.EnableOutput.vi"/>
		<Item Name="Probus.SetCurrent.vi" Type="VI" URL="../Probus.SetCurrent.vi"/>
		<Item Name="Probus.SetVoltage.vi" Type="VI" URL="../Probus.SetVoltage.vi"/>
		<Item Name="Probus.Transaction.vi" Type="VI" URL="../Probus.Transaction.vi"/>
		<Item Name="Probus.NegPolarity.vi" Type="VI" URL="../Probus.NegPolarity.vi"/>
		<Item Name="Probus.PosPolarity.vi" Type="VI" URL="../Probus.PosPolarity.vi"/>
		<Item Name="Probus.TransactionTest.vi" Type="VI" URL="../Probus.TransactionTest.vi"/>
	</Item>
	<Item Name="Probus.VI_Tree.vi" Type="VI" URL="../Probus.VI_Tree.vi"/>
	<Item Name="Readme.txt" Type="Document" URL="../Readme.txt"/>
</Library>
